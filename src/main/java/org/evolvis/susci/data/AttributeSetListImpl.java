package org.evolvis.susci.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AttributeSetListImpl implements AttributeSetList {
	private final List<AttributeSet> content;

	public AttributeSetListImpl() {
		this.content = new ArrayList<AttributeSet> ();
	}

	public AttributeSetListImpl(List<AttributeSet>  content) {
		this.content = new ArrayList<AttributeSet> (content);
	}

	public int size() {
		return content.size();
	}

	public boolean isEmpty() {
		return content.isEmpty();
	}

	public AttributeSet get(int index) {
		return content.get(index);
	}

	public void add(AttributeSet attributeSet) {
		content.add(attributeSet);
	}

	public Iterator<AttributeSet> iterator() {
		return content.iterator();
	}

	public List<AttributeSet> asList() {
		return new ArrayList<AttributeSet>(content);
	}

	@Override
	public String toString() {
		return content.toString();
	}
}
