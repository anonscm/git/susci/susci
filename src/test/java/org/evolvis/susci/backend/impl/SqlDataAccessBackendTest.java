/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.susci.backend.impl;

import java.io.InputStream;
import java.sql.SQLException;

import junit.framework.TestCase;

import org.evolvis.susci.DataAccess;
import org.evolvis.susci.DataAccessConfiguration;
import org.evolvis.susci.DataAccessException;
import org.evolvis.susci.Susci;
import org.evolvis.susci.data.AttributeSet;
import org.evolvis.susci.data.AttributeSetList;
import org.evolvis.susci.test.Address;
import org.evolvis.susci.test.Person;

public class SqlDataAccessBackendTest extends TestCase {
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		InputStream inputStream = SqlDataAccessBackendTest.class.
				getResourceAsStream("hsqldb-test-dataaccess.xml");
		
		new DataAccessConfiguration().parseXmlConfig(inputStream);
	}

	public void testCorrectBackend() {
		DataAccess dataAccess = new DataAccess("hsqldb-memory01");
		
		SqlDataAccessBackend backend = (SqlDataAccessBackend) dataAccess.getBackend();
		
		backend.store(null, "CREATE TABLE users (" +
				"uid VARCHAR," +
				"username VARCHAR," +
				"password VARCHAR)");
		
		backend.store(null, "INSERT INTO users VALUES ('1', 'abc', 'abc')");
		backend.store(null, "INSERT INTO users VALUES ('2', 'def', 'def')");
		
		AttributeSetList result = (AttributeSetList) backend.executeQuery(null, "SELECT * FROM users");
		
		assertEquals(2, result.size());
		
		AttributeSet entry1 = result.get(0);
		AttributeSet entry2 = result.get(1);
		
		assertEquals("1", entry1.getAttribute("UID"));
		assertEquals("abc", entry1.getAttribute("USERNAME"));
		assertEquals("abc", entry1.getAttribute("PASSWORD"));
		
		assertEquals("2", entry2.getAttribute("UID"));
		assertEquals("def", entry2.getAttribute("USERNAME"));
		assertEquals("def", entry2.getAttribute("PASSWORD"));
	}
	
	public void testCreateAndDropTable() {
		DataAccess dataAccess = new DataAccess("hsqldb-memory01");
		
		SqlDataAccessBackend backend = (SqlDataAccessBackend) dataAccess.getBackend();
		
		backend.createTable(dataAccess.getBackend().getBackendName(), Person.class);
		backend.createTable(dataAccess.getBackend().getBackendName(), Address.class);
		
		try {
			backend.createTable(dataAccess.getBackend().getBackendName(), Person.class);
		} catch (DataAccessException e) {
			assertEquals(SQLException.class, e.getCause().getClass());
		}

		try {
			backend.createTable(dataAccess.getBackend().getBackendName(), Address.class);
		} catch (DataAccessException e) {
			assertEquals(SQLException.class, e.getCause().getClass());
		}
		
		backend.dropTable(dataAccess.getBackend().getBackendName(), Person.class);
		backend.dropTable(dataAccess.getBackend().getBackendName(), Address.class);

		try {
			backend.dropTable(dataAccess.getBackend().getBackendName(), Person.class);
		} catch (DataAccessException e) {
			assertEquals(SQLException.class, e.getCause().getClass());
		}

		try {
			backend.dropTable(dataAccess.getBackend().getBackendName(), Address.class);
		} catch (DataAccessException e) {
			assertEquals(SQLException.class, e.getCause().getClass());
		}
	}

	public void testASD() {
		DataAccess dataAccess = new DataAccess("hsqldb-memory01");
		
		SqlDataAccessBackend backend = (SqlDataAccessBackend) dataAccess.getBackend();
		
		backend.createTable(dataAccess.getBackend().getBackendName(), Person.class);
		backend.createTable(dataAccess.getBackend().getBackendName(), Address.class);

		Person jim = new Person();
		jim.setFirstname("Jim");
		jim.setLastname("Panse");
		
		Person bernhard = new Person();
		bernhard.setFirstname("Bernhard");
		bernhard.setLastname("Diener");
		
		Person peter = new Person();
		peter.setFirstname("Peter");
		peter.setLastname("Silie");
		
		dataAccess.store(jim);
		dataAccess.store(bernhard);
		dataAccess.store(peter);
		
		backend.commit();
		
		Person person1 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Jim"));
		Person person2 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Bernhard"));
		Person person3 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Peter"));
		Person person4 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Dieter"));
		
		assertNotNull(person1);
		assertNotNull(person2);
		assertNotNull(person3);
		assertNull(person4);
		
		dataAccess.delete(person1);
		dataAccess.delete(person2);
		
		person1 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Jim"));
		person2 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Bernhard"));
		person3 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Peter"));
		person4 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Dieter"));
		
		assertNotNull(person1);
		assertNotNull(person2);
		assertNotNull(person3);
		assertNull(person4);
		
		dataAccess.commit();

		person1 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Jim"));
		person2 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Bernhard"));
		person3 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Peter"));
		person4 = dataAccess.getEntry(Person.class, Susci.luceneQuery("firstname:Dieter"));
		
		assertNull(person1);
		assertNull(person2);
		assertNotNull(person3);
		assertNull(person4);
		
		backend.dropTable(dataAccess.getBackend().getBackendName(), Person.class);
		backend.dropTable(dataAccess.getBackend().getBackendName(), Address.class);
	}
}
