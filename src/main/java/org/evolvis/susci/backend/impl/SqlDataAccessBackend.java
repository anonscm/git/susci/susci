/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.susci.backend.impl;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;

import org.evolvis.susci.DataAccessException;
import org.evolvis.susci.QueryProcessor;
import org.evolvis.susci.StoreProcessor;
import org.evolvis.susci.backend.AbstractDataAccessBackend;
import org.evolvis.susci.backend.QueryingStrategy;
import org.evolvis.susci.backend.QueryingWithQueryBuilder;
import org.evolvis.susci.backend.StoringAttributeSets;
import org.evolvis.susci.backend.StoringStrategy;
import org.evolvis.susci.config.ConfigFactory;
import org.evolvis.susci.config.MappingRules;
import org.evolvis.susci.data.AttributeSet;
import org.evolvis.susci.data.AttributeSetImpl;
import org.evolvis.susci.data.AttributeSetList;
import org.evolvis.susci.data.AttributeSetListImpl;
import org.evolvis.susci.query.QueryBuilder;
import org.evolvis.susci.query.impl.SqlQueryBuilder;

import de.tarent.commons.utils.Pojo;

public class SqlDataAccessBackend extends AbstractDataAccessBackend implements QueryingWithQueryBuilder, StoringAttributeSets {
	private Connection connection;

	private String driverClass;
	private String jdbcURL;
	private String username;
	private String password;

	private boolean usePreparedStatementOnInsert = true;
	private boolean usePreparedStatementOnUpdate = true;
	private boolean usePreparedStatementOnDelete = true;

	private Class requestedType;

	public void init() {
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getJdbcURL() {
		return jdbcURL;
	}

	public void setJdbcURL(String jdbcURL) {
		this.jdbcURL = jdbcURL;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public QueryingStrategy getQueryingStrategy() {
		return this;
	}

	public StoringStrategy getStoringStrategy() {
		return this;
	}

	public QueryBuilder getQueryBuilder() {
		return new SqlQueryBuilder();
	}

	public Object executeQuery(QueryProcessor queryProcessor, Object query) {
		if (!(query instanceof String))
			throw new IllegalArgumentException("Only pur strings are (currently) allowed for storing, please use pure SQL.");

		StringBuffer sql = new StringBuffer();
		if (queryProcessor != null) {
			sql.append("SELECT * FROM ");
			sql.append(getTableName(queryProcessor));
			if (query != null && ((String) query).length() != 0)
				sql.append(" ").append(query);
		} else {
			sql.append(query);
		}

		Statement s = null;
		try {
			ensureOpenConnection();
			s = connection.createStatement();

			AttributeSetList result = new AttributeSetListImpl();
			
			ResultSet resultSet = s.executeQuery(sql.toString());
			ResultSetMetaData metaData = resultSet.getMetaData();

			while (resultSet.next()) {
				AttributeSet attributeSet = new AttributeSetImpl();
				for (int i = 1; i <= metaData.getColumnCount(); i++) {
					attributeSet.setAttribute(metaData.getColumnName(i), resultSet.getObject(i));
				}
				result.add(attributeSet);
			}

			return result;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	/** {@inheritDoc} */
	public void store(StoreProcessor storeProcessor, Object o) {
		if (!(o instanceof String))
			throw new IllegalArgumentException("Only pur strings are (currently) allowed for storing, please use pure SQL.");

		Statement s = null;
		try {
			ensureOpenConnection();
			s = connection.createStatement();
			s.executeUpdate((String) o);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	public void store(StoreProcessor storeProcessor, AttributeSet attributeSet) {
		MappingRules mappingRules = storeProcessor.getBackendName() == null ? null : ConfigFactory.getMappingConfig().getRules(storeProcessor.getBackendName(), storeProcessor.getAffectedType().getName());
		
		boolean insert = getPrimaryKeyFromAttributeSet(storeProcessor, attributeSet) == null;

		StringBuffer sql = new StringBuffer();

		if (insert) {
			sql.append("INSERT INTO ");
			sql.append(getTableName(storeProcessor));
			sql.append(" (");
			for (Iterator it = attributeSet.getAttributeNames().iterator(); it.hasNext();) {
				String column = mappingRules.transformResultToBackend((String) it.next());
				if (column.equals(getPrimaryKey(storeProcessor)))
					continue;
				sql.append(column).append(it.hasNext() ? ", " : ") ");
			}
			sql.append("VALUES (");
			for (Iterator it = attributeSet.getAttributeNames().iterator(); it.hasNext();) {
				String column = mappingRules.transformResultToBackend((String) it.next());
				if (column.equals(getPrimaryKey(storeProcessor)))
					continue;
				sql.append(it.hasNext() ? "?, " : "?)");
			}
		} else {
			sql.append("UPDATE TABLE ");
			sql.append(getTableName(storeProcessor));
			sql.append("SET ");
			for (Iterator it = attributeSet.getAttributeNames().iterator(); it.hasNext();) {
				String column = mappingRules.transformResultToBackend((String) it.next());
				if (column.equals(getPrimaryKey(storeProcessor)))
					continue;
				sql.append(column).append(" = ?");
			}
			sql.append("WHERE");
			sql.append(getPrimaryKey(storeProcessor)).append(" = ?");
		}
		
		PreparedStatement s = null;
		try {
			ensureOpenConnection();
			s = connection.prepareStatement(sql.toString());
			int parameterIndex = 1;
			for (Iterator it = attributeSet.getAttributeNames().iterator(); it.hasNext();) {
				String column = mappingRules.transformResultToBackend((String) it.next());
				if (!column.equals(getPrimaryKey(storeProcessor)))
					s.setObject(parameterIndex++, attributeSet.getAttribute(column));
				else if (!insert)
					s.setObject(attributeSet.getAttributeNames().size(), attributeSet.getAttribute(column));
			}
			s.execute();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	public void delete(StoreProcessor storeProcessor, AttributeSet attributeSet) {
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM ");
		sql.append(getTableName(storeProcessor));
		sql.append(" WHERE ");
		sql.append(getPrimaryKey(storeProcessor)).append(" = ?");

		PreparedStatement s = null;
		try {
			ensureOpenConnection();
			s = connection.prepareStatement(sql.toString());
			s.setObject(1, attributeSet.getAttribute(getPrimaryKey(storeProcessor)));
			s.execute();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	private String getPrimaryKeyFromAttributeSet(StoreProcessor storeProcessor, AttributeSet attributeSet) {
		return (String) attributeSet.getAttribute(getPrimaryKey(storeProcessor));
	}

	private String getTableName(QueryProcessor queryProcessor) {
		Map parameters = ConfigFactory.getMappingConfig().getParameters(queryProcessor.getBackendName(), queryProcessor.getRequestedType().getName());
		if (parameters == null)
			throw new IllegalArgumentException("No parameters configured for the backend " + queryProcessor.getBackendName() + " and the class name " + queryProcessor.getRequestedType().getName() + ". Need 'tablename'.");
		String tablename = (String) parameters.get("tablename");
		if (tablename == null || tablename.length() == 0)
			throw new IllegalArgumentException("No parameter 'tablename' for the backend " + queryProcessor.getBackendName() + " and the class name " + queryProcessor.getRequestedType().getName() + " found.");
		return tablename;
	}

	private String getTableName(StoreProcessor storeProcessor) {
		Map parameters = ConfigFactory.getMappingConfig().getParameters(storeProcessor.getBackendName(), storeProcessor.getAffectedType().getName());
		if (parameters == null)
			throw new IllegalArgumentException("No parameters configured for the backend " + storeProcessor.getBackendName() + " and the class name " + storeProcessor.getAffectedType().getName() + ". Need 'tablename'.");
		String tablename = (String) parameters.get("tablename");
		if (tablename == null || tablename.length() == 0)
			throw new IllegalArgumentException("No parameter 'tablename' for the backend " + storeProcessor.getBackendName() + " and the class name " + storeProcessor.getAffectedType().getName() + " found.");
		return tablename;
	}

	private String getPrimaryKey(StoreProcessor storeProcessor) {
		Map parameters = ConfigFactory.getMappingConfig().getParameters(storeProcessor.getBackendName(), storeProcessor.getAffectedType().getName());
		if (parameters == null)
			throw new IllegalArgumentException("No parameters configured for the backend " + storeProcessor.getBackendName() + " and the class name " + storeProcessor.getAffectedType().getName() + ". Need primary 'key'.");
		String key = (String) parameters.get("key");
		if (key == null || key.length() == 0)
			throw new IllegalArgumentException("No primary 'key' for the backend " + storeProcessor.getBackendName() + " and the class name " + storeProcessor.getAffectedType().getName() + " found.");
		return key;
	}

	public void createTable(String backendName, Class c) {
		MappingRules mappingRules = backendName == null ? null : ConfigFactory.getMappingConfig().getRules(backendName, c.getName());
		Map parameters = backendName == null ? null : ConfigFactory.getMappingConfig().getParameters(backendName, c.getName());
		String tableName = parameters == null ? null : (String) parameters.get("tablename");
		if (tableName == null)
			tableName = c.getSimpleName();

		StringBuffer sql = new StringBuffer();
		sql.append("CREATE TABLE ");
		sql.append(tableName);
		sql.append(" (");

		Map propertyTypes = Pojo.getReadablePropertyTypes(c);
		for (Iterator propertyIterator = propertyTypes.entrySet().iterator(); propertyIterator.hasNext();) {
			Map.Entry entry = (Map.Entry) propertyIterator.next();
			String property = mappingRules.transformResultToBackend((String) entry.getKey());
			Class type = (Class) entry.getValue();
			if (property == null)
				continue;

			sql.append(property).append(" ");

			if (type.isAssignableFrom(String.class)) {
				sql.append("VARCHAR");
				// } else if (type.isAssignableFrom(Number.class)) {
				// createStatement.append("VARCHAR");
			} else {
				throw new IllegalArgumentException("Unsupported " + c + ".");
			}

			sql.append(propertyIterator.hasNext() ? ", " : ")");
		}

		Statement s = null;
		try {
			ensureOpenConnection();
			s = connection.createStatement();
			s.executeUpdate(sql.toString());
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	public void dropTable(String backendName, Class c) {
		Map parameters = backendName == null ? null : ConfigFactory.getMappingConfig().getParameters(backendName, c.getName());
		String tableName = parameters == null ? null : (String) parameters.get("tablename");
		if (tableName == null)
			tableName = c.getSimpleName();

		StringBuffer createStatement = new StringBuffer();
		createStatement.append("DROP TABLE ");
		createStatement.append(tableName);
		createStatement.append(";");

		Statement s = null;
		try {
			ensureOpenConnection();
			s = connection.createStatement();
			s.executeUpdate(createStatement.toString());
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			close(s);
		}
	}

	/** {@inheritDoc} */
	public void begin() {
		try {
			ensureOpenConnection();
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** {@inheritDoc} */
	public void commit() {
		try {
			ensureOpenConnection();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** {@inheritDoc} */
	public void rollback() {
		try {
			ensureOpenConnection();
			connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void ensureOpenConnection() {
		if (connection == null) {
			try {
				if (driverClass != null) {
					DriverManager.registerDriver((Driver) Class.forName(driverClass).newInstance());
				}
			} catch (Exception e) {
				connection = null;
				throw new DataAccessException(e);
			}
			try {
				connection = DriverManager.getConnection(jdbcURL, username, password);
				connection.setAutoCommit(false);
			} catch (SQLException e) {
				connection = null;
				throw new DataAccessException(e);
			}
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new DataAccessException(e);
			}
		}
	}

	private void close(Statement statement) {
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}

	public Class getRequestedType() {
		return requestedType;
	}

	public void setRequestedType(Class requestedType) {
		this.requestedType = requestedType;
	}
}
