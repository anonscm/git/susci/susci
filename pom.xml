<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>org.evolvis.susci</groupId>

	<artifactId>susci</artifactId>

	<packaging>jar</packaging>

	<version>1.0.1-SNAPSHOT</version>

	<name>susci</name>

	<description>An small data access library.</description>

	<url>http://susci.evolvis.org/</url>

	<inceptionYear>2007</inceptionYear>

	<licenses>
		<license>
			<name>GNU General Public License</name>
			<url>http://www.gnu.org/licenses/gpl-2.0.txt</url>
		</license>
	</licenses>

	<organization>
		<name>tarent GmbH, Germany</name>
		<url>http://www.tarent.de/</url>
	</organization>

	<scm>
		<url>http://evolvis.org/plugins/scmsvn/viewcvs.php/trunk/?root=susci</url>
		<connection>scm:svn:svn+ssh://maven@svn.evolvis.org/svnroot/susci/trunk</connection>
	</scm>

	<mailingLists>
		<mailingList>
			<name>susci-commits</name>
			<post>susci-commits@lists.evolvis.org</post>
			<archive>
				http://lists.evolvis.org/pipermail/susci-commits/
			</archive>
			<subscribe>
				http://lists.evolvis.org/cgi-bin/mailman/listinfo/susci-commits
			</subscribe>
			<unsubscribe>
				http://lists.evolvis.org/cgi-bin/mailman/listinfo/susci-commits
			</unsubscribe>
		</mailingList>
	</mailingLists>

	<ciManagement>
		<system>continuum</system>
		<url>http://cis.evolvis.org:8080/continuum/</url>
	</ciManagement>

	<issueManagement>
		<system>gforge</system>
		<url>
			http://evolvis.org/tracker/?atid=190&amp;group_id=35&amp;func=browse
		</url>
	</issueManagement>

	<repositories>
		<repository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>http://maven-repo.evolvis.org/releases</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>http://maven-repo.evolvis.org/snapshots</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>http://maven-repo.evolvis.org/releases</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
		<pluginRepository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>http://maven-repo.evolvis.org/snapshots</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>false</enabled>
			</releases>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<repository>
			<id>evolvis-release-repository</id>
			<name>evolvis.org release repository</name>
			<url>
				scpexe://maven-repo.evolvis.org/var/www/maven_repo/releases
			</url>
		</repository>
		<snapshotRepository>
			<id>evolvis-snapshot-repository</id>
			<name>evolvis.org snapshot repository</name>
			<url>
				scpexe://maven-repo.evolvis.org/var/www/maven_repo/snapshots
			</url>
		</snapshotRepository>
		<site>
			<id>evolvis-site-repository</id>
			<name>evolvis.org site repository</name>
			<url>
				scpexe://maven@svn.evolvis.org/site/susci/htdocs/${project.version}
			</url>
		</site>
	</distributionManagement>

	<developers>
		<developer>
			<id>jerolimov</id>
			<name>Christoph Jerolimov</name>
			<email>c.jerolimov@tarent.de</email>
			<organization>tarent GmbH</organization>
			<organizationUrl>http://www.tarent.de/</organizationUrl>
			<roles>
				<role>Developer</role>
			</roles>
			<timezone>+1</timezone>
		</developer>
	</developers>

	<build>
		<sourceDirectory>src/main/java</sourceDirectory>
		<testSourceDirectory>src/test/java</testSourceDirectory>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
			</resource>
		</resources>
		<testResources>
			<testResource>
				<directory>src/test/resources</directory>
			</testResource>
		</testResources>
		<outputDirectory>target/classes</outputDirectory>
		<testOutputDirectory>target/test-classes</testOutputDirectory>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.5</source>
					<target>1.5</target>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifestEntries>
							<Implementation-Title>
								${project.artifactId}
							</Implementation-Title>
							<version_description>
								${project.description}
							</version_description>
							<version_copyright>
								Copyright (c) 2007 tarent GmbH
							</version_copyright>
							<Implementation-Vendor>
								tarent GmbH (www.tarent.de)
							</Implementation-Vendor>
							<Implementation-Version>
								${project.version}
							</Implementation-Version>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>true</downloadJavadocs>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>
					maven-project-info-reports-plugin
				</artifactId>
				<!--
					<reportSets>
					<reportSet>
					<reports>
					<report>dependencies</report>
					<report>project-team</report>
					<report>mailing-list</report>
					<report>cim</report>
					<report>issue-tracking</report>
					<report>license</report>
					<report>scm</report>
					</reports>
					</reportSet>
					</reportSets>
				-->
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>
					maven-project-info-reports-plugin
				</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clover-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changes-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changelog-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<source>1.5</source>
					<target>1.5</target>
					<encoding>UTF-8</encoding>
					<charset>UTF-8</charset>
					<minmemory>128m</minmemory>
					<maxmemory>256m</maxmemory>
					<show>package</show>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>2.0</version>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdepend-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>javancss-maven-plugin</artifactId>
				<version>2.0-beta-1</version>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>1.0.0</version>
			</plugin>

			<!--
				<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdiff-maven-plugin</artifactId>
				<version>0.1-SNAPSHOT</version>
				<configuration>
				<packages>net.sf.json</packages>
				<oldTag>2.0</oldTag>
				<newTag>CURRENT</newTag>
				</configuration>
				</plugin>
			-->
		</plugins>
	</reporting>

	<dependencies>
		<!--
			The currently real dependency is tarent-commons. We will use only
			these ConverterRegistry, BeanAccessor, EntityFactory, Pojo and
			dependend classes.
		-->
		<dependency>
			<groupId>de.tarent.libraries</groupId>
			<artifactId>tarent-commons</artifactId>
			<version>1.3.4</version>
		</dependency>

		<!-- As primary logging interface. -->
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.0.4</version>
		</dependency>

		<!-- For BrokerDataAccessBackend and BrokerQueryBuilder -->
		<dependency>
			<groupId>de.tarent.octopus</groupId>
			<artifactId>octopus-core</artifactId>
			<version>1.90.6</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>

		<!-- For BrokerDataAccessBackend and BrokerQueryBuilder -->
		<dependency>
			<groupId>de.tarent.octopus</groupId>
			<artifactId>octopus-rpctunnel</artifactId>
			<version>0.2</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>

		<!-- For LuceneQueryBuilder and LuceneQueryParser -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-core</artifactId>
			<version>2.2.0</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>

		<!-- For configuration reading and writing -->
		<!-- Will be removed soon! -->
		<dependency>
			<groupId>com.thoughtworks.xstream</groupId>
			<artifactId>xstream</artifactId>
			<version>1.2.2</version>
		</dependency>

		<!-- For testing -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>

		<!-- For testing SqlDataAccessBackend -->
		<dependency>
			<groupId>hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<version>1.8.0.7</version>
		</dependency>
	</dependencies>

	<properties>
		<maven.test.skip>true</maven.test.skip>
	</properties>
</project>