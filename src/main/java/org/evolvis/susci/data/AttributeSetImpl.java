/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.susci.data;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AttributeSetImpl implements AttributeSet {
	private final Map<String, Object> content = new LinkedHashMap<String, Object>();
	private final List<AttributeListener> listener = new LinkedList<AttributeListener>();

	public List<String> getAttributeNames() {
		return Arrays.asList(content.keySet().toArray(new String[content.size()]));
	}

	public boolean containsAttribute(String attributeName) {
		return content.containsKey(attributeName);
	}

	public void setAttribute(String attributeName, Object attributeValue) {
		Object oldValue = content.put(attributeName, attributeValue);
		if (!listener.isEmpty())
			for (AttributeListener l : listener)
				l.handleAttributeChange(attributeName, oldValue, attributeValue);
	}

	public Object getAttribute(String attributeName) {
		return content.get(attributeName);
	}

	public void addAttributeListener(AttributeListener attributeListener) {
		listener.add(attributeListener);
	}

	public void removeAttributeListener(AttributeListener attributeListener) {
		listener.remove(attributeListener);
	}

	public int compareTo(AttributeSet other) {
		AttributeSet otherSet = other;
		
		if (content.size() < otherSet.getAttributeNames().size())
			return -1;
		else if (content.size() > otherSet.getAttributeNames().size())
			return 1;
		
		for (Map.Entry<String, Object> entry : content.entrySet()) {
			Object otherValue = otherSet.getAttribute(entry.getKey());
			if (entry.getValue() == null && otherValue == null)
				continue;
			else if (entry.getValue() == null && otherValue != null)
				return -1;
			else if (entry.getValue() != null && otherValue == null)
				return 1;
			
			if (entry.getValue() instanceof Comparable) {
				Comparable<Object> c = (Comparable<Object>) entry.getValue();
				return c.compareTo(otherValue);
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof AttributeSet) {
			AttributeSet otherSet = (AttributeSet) other;
			if (content.size() != otherSet.getAttributeNames().size())
				return false;
			
			for (Map.Entry<String, Object> entry : content.entrySet()) {
				Object otherValue = otherSet.getAttribute(entry.getKey());
				if (entry.getValue() == null && otherValue != null)
					return false;
				else if (entry.getValue() != null && otherValue == null)
					return false;
				else if (entry.getValue() != null && !entry.getValue().equals(otherValue))
					return false;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return content.hashCode();
	}

	@Override
	public String toString() {
		return content.toString();
	}
}
