/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.susci;

import java.util.LinkedList;
import java.util.List;

import de.tarent.commons.utils.Converter;
import de.tarent.commons.utils.ConverterRegistry;

class DataAccessConverterRegistry {
	private static final ConverterRegistry instance;

	static {
		instance = ConverterRegistry.createPrefilledRegistry();
		instance.register(new StringToListConverter(), true);
	}

	static ConverterRegistry getInstance() {
		return instance;
	}

	private static class StringToListConverter implements Converter {
		public Object convert(Object sourceData) throws IllegalArgumentException {
			LinkedList linkedList = new LinkedList();
			linkedList.addFirst(sourceData);
			return linkedList;
		}

		public String getConverterName() {
			return "StringToListConverter";
		}

		public Class getSourceType() {
			return String.class;
		}

		public Class getTargetType() {
			return List.class;
		}
	}
}
