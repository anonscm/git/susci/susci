/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.susci.query.impl;

import org.evolvis.susci.query.AbstractQueryContext;
import org.evolvis.susci.query.AbstractVisitorListener;
import org.evolvis.susci.query.QueryBuilder;
import org.evolvis.susci.query.QueryVisitor;


/**
 * <p>This {@link QueryBuilder} generates an LDAP query string.</p>
 * 
 * <p>See <a href="http://java.sun.com/products/jndi/tutorial/basics/directory/filter.html">
 * Sun JNDI LDAP Tutorial</a> and <a href="http://www.ietf.org/rfc/rfc2254.txt">
 * RFC 2254</a> for more informations.</p>
 * 
 * @author Christoph Jerolimov, tarent GmbH
 */
public class LdapQueryBuilder extends AbstractQueryContext implements QueryBuilder {
	private final StringBuffer stringBuffer = new StringBuffer();

	public LdapQueryBuilder() {
	}

	@Override
	public void setQueryVisitor(QueryVisitor queryVisitor) {
		if (!(queryVisitor instanceof TraversingVisitor))
			throw new IllegalArgumentException("Can only interact with TraversingVisitor.");
		
		TraversingVisitor traversingVisitor = (TraversingVisitor)queryVisitor;
		
		traversingVisitor.setEmptyVisitorListener(null);
		traversingVisitor.setTermVisitorListener(null);
		traversingVisitor.setAndVisitorListener(new AndVisitorListener());
		traversingVisitor.setOrVisitorListener(new OrVisitorListener());
		traversingVisitor.setXorVisitorListener(new XorVisitorListener());
		traversingVisitor.setNotVisitorListener(new NotVisitorListener());
	}

	public void filterAttributeEqualWithPattern(String attribute, String pattern) {
		stringBuffer.append(attribute).append("=").append(pattern);
	}

	public void filterAttributeEqualWithOneOf(String attribute, String[] values) {
		stringBuffer.append("(|");
		for (int i = 0; i < values.length; i++) {
			stringBuffer.
					append("(").
					append(attribute).
					append("=").
					append(values[i]).
					append(")");
		}
		stringBuffer.append(")");
	}

	public void filterAttributeIsNull(String attribute) {
		stringBuffer.append("(!(").append(attribute).append("=*))");
	}

	public void filterAttributeIsNotNull(String attribute) {
		stringBuffer.append(attribute).append("=*");
	}

	public void filterAttributeLowerThanPattern(String attribute, String pattern) {
		throw new IllegalArgumentException(
				"LDAP does not support lower than pattern, " +
				"use lower or equal instead.");
	}

	public void filterAttributeLowerOrEqualsThanPattern(String attribute, String pattern) {
		stringBuffer.append(attribute).append("<=").append(pattern);
	}

	public void filterAttributeGreaterThanPattern(String attribute, String pattern) {
		throw new IllegalArgumentException(
				"LDAP does not support greater than pattern, " +
				"use greater or equal instead.");
	}

	public void filterAttributeGreaterOrEqualsThanPattern(String attribute, String pattern) {
		stringBuffer.append(attribute).append(">=").append(pattern);
	}

	private class AndVisitorListener extends AbstractVisitorListener {
		@Override
		public void handleBeforeAll(int entryCount) {
			stringBuffer.append("(&(");
		}
		
		@Override
		public void handleBeforeEntry(boolean first, boolean last) {
		}
		
		@Override
		public void handleAfterEntry(boolean first, boolean last) {
			stringBuffer.append(last ? ")" : ")(");
		}
		
		@Override
		public void handleAfterAll(int entryCount) {
			stringBuffer.append(")");
		}
	}

	private class OrVisitorListener extends AbstractVisitorListener {
		@Override
		public void handleBeforeAll(int entryCount) {
			stringBuffer.append("(|(");
		}
		
		@Override
		public void handleBeforeEntry(boolean first, boolean last) {
		}
		
		@Override
		public void handleAfterEntry(boolean first, boolean last) {
			stringBuffer.append(last ? ")" : ")(");
		}
		
		@Override
		public void handleAfterAll(int entryCount) {
			stringBuffer.append(")");
		}
	}

	private class XorVisitorListener extends AbstractVisitorListener {
		@Override
		public void handleBeforeAll(int entryCount) {
			stringBuffer.append("(^(");
		}
		
		@Override
		public void handleBeforeEntry(boolean first, boolean last) {
		}
		
		@Override
		public void handleAfterEntry(boolean first, boolean last) {
			stringBuffer.append(last ? ")" : ")(");
		}
		
		@Override
		public void handleAfterAll(int entryCount) {
			stringBuffer.append(")");
		}
	}

	private class NotVisitorListener extends AbstractVisitorListener {
		@Override
		public void handleBeforeAll(int entryCount) {
			stringBuffer.append("(!(");
		}
		
		@Override
		public void handleBeforeEntry(boolean first, boolean last) {
		}
		
		@Override
		public void handleAfterEntry(boolean first, boolean last) {
			stringBuffer.append(last ? ")" : ")(");
		}
		
		@Override
		public void handleAfterAll(int entryCount) {
			stringBuffer.append(")");
		}
	}

	public String getExpression() {
		return stringBuffer.toString();
	}

	public Object getQuery() {
		return getExpression();
	}
}
